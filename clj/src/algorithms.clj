(ns algorithms
  (:gen-class))

(def fib
  (memoize
   (fn [x]
      (if (< x 2)
        x
        (+ (fib (dec (dec x))) (fib (dec x)))))))

(def fib-seq
  ((fn rfib [a b]
     (lazy-seq (cons a (rfib b (+ a b)))))
   0 1))

;A version with recursion on data, not functions. see http://en.wikipedia.org/wiki/Corecursion :
(def fib-seq
  (lazy-cat [0 1] (map + (rest fib-seq) fib-seq)))

; tail recursive fibo
(defn tail-fibo [n]
  (letfn [(in-fib [current next n]
            (if (zero? n)
              current
              (in-fib next (+ current next) (dec n))))]
    (in-fib 0N 1N n)))

(defn recur-fib [n]
  (letfn [(inner-fib [current next n]
            (if (zero? n)
              current
              (recur next (+ current next) (dec n))))]
    (inner-fib 0N 1N n)))

(defn uber-optimzed-fib [n]
  (reduce
    (fn [a _]
      (conj a (+ (nth a (- (count a) 1)) (nth a (- (count a) 2)))))
    [0 1 1] (rest (rest (range n)))))

;; compute a huge fibanacci number modulo m
(defn fib-last-number [n]
  (last (reduce
          (fn [a _]
            (let [res (+ (nth a (- (count a) 1)) (nth a (- (count a) 2)))]
              (conj a (mod res 10))))
          [0 1 1] (rest (rest (range n))))))

(defn sum-of-two [coll]
  (reduce + coll))

;; need to investidate (map + a (next a) (nnext a))
(defn maximum-pairwise-product [coll]
  (let [res (map * coll (next coll))
        ;res (map (partial apply *) (partition 2 1 coll))
        sorted (reverse (sort res))]
    (first sorted)))

(defn largest-number-for-amount [budget, items]
  (let [filtered (filter #(> budget %) items)
        sorted (sort filtered)
        res (take-while (partial >= budget) (reductions + sorted))]
    (count res)))

;; The Euclidean algorithm which finds the highest common factor by using the modulus function
;; so swap a and b if a is greater find the remainder of dividing the larger number by the smaller
;; and then use the smaller num,ber as the bigger number and the remainder of the modulo operation
;; as the smaller number and continue until the smaller number == 0;
(defn greatest-common-factor-euclid [a b]
      (if (= 0 b) a
        (let [res (if (> a b) [b a] [a b])
              ;; second == larger number first == smaller number
              remainder (mod (second res) (first res))]
             (greatest-common-factor-euclid (first res) remainder))))

;; The best way to find the lowest common multiple is to multiply the number together then
;; divide that result by the lowest common factor found by using the super fast Euclidean algorithm
(defn lowest-common-multiple [a b]
  (/ (* a b) (greatest-common-factor-euclid a b)))

(defn -main [& args]
  (sum-of-two [1 9 82 13 12]))

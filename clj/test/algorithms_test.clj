(ns algorithms-test
  (:require [clojure.test :refer :all])
  (:use [algorithms]))

(deftest sum-of-two-test
  (is (= 12 (sum-of-two [4 2 3 3]))))

(deftest maximum-pairwise-product-test
  (is (= 6 (maximum-pairwise-product [2 3 1 4 1]))))

(deftest fibo-test
  (is (= 34N (tail-fibo 9))))

(deftest recur-fibo-test
  (is (= 34N (recur-fib 9))))

(deftest memo-fib-test
  (is (= 34N (fib 9))))

(deftest uber-optimzed-fib-test
  (is (= [0 1 1 2 3 5 8 13 21 34] (uber-optimzed-fib 9))))

(deftest largest-number-for-amount-test
  (is (= 4 (largest-number-for-amount 20 [55 7 3 5 19 44 18 5]))))

(deftest last-fib-number
  (is (= 2 (fib-last-number 3))))

(deftest last-fib-number-large
  (is (= 9 (fib-last-number 331))))

(deftest last-fib-number-huge
  (is (= 5 (fib-last-number 327305))))

(deftest highest-common-factor-test
  (is (= 5 (greatest-common-factor-euclid 15 10))))

(deftest highest-common-factor-reversed-test
  (is (= 5 (greatest-common-factor-euclid 10 15))))

(deftest highest-common-factor-another-test
  (is (= 3 (greatest-common-factor-euclid 33 27))))

(deftest lowest-common-multiple-simple-test
  (is (= 12 (lowest-common-multiple 3 4))))

(deftest lowest-common-multiple-simple-direct-test
  (is (= 6 (lowest-common-multiple 6 2))))

(deftest lowest-common-multiple-simple-large-test
  (is (= 1933053046 (lowest-common-multiple 28851538 1183019))))


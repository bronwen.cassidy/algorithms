(defproject
  clj
  "1.0.0-SNAPSHOT"
  :desciption "clojure algorithms implemented"
  :repositories
  [["clojars" {:url "https://repo.clojars.org/"}]
   ["maven-central" {:url "https://repo1.maven.org/maven2"}]]
  :dependencies [[org.clojure/clojure "1.10.0"]]
  :source-paths
  ["src/"]
  :test-paths
  ["test/"]
  :resource-paths
  ["resources/"])

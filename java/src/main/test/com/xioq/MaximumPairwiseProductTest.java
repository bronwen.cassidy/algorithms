package com.xioq;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class MaximumPairwiseProductTest {

    @Test
    public void testExecute() {
        MaximumPairwiseProduct mpp = new MaximumPairwiseProduct();
        List<Integer> items = Arrays.asList(9, 22, 5, 4, 44, 21, 75);
        long expected = 21 * 75;
        long actual = mpp.execute(items);
        assertEquals(expected, actual);
    }

    @Test
    public void testExecuteFirst() {
        MaximumPairwiseProduct mpp = new MaximumPairwiseProduct();
        List<Integer> items = Arrays.asList(900, 22, 5, 4, 44, 21, 75, 5);
        long expected = 900 * 22;
        long actual = mpp.execute(items);
        assertEquals(expected, actual);
    }

    @Test
    public void testExecuteMiddle() {
        MaximumPairwiseProduct mpp = new MaximumPairwiseProduct();
        List<Integer> items = Arrays.asList(9, 22, 5, 4, 99, 44, 21, 75, 5);
        long expected = 99 * 44;
        long actual = mpp.execute(items);
        assertEquals(expected, actual);
    }
}
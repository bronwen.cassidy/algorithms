package com.xioq;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class SumOfTwoNumbersTest {

    @Test
    public void testSumOfTwoNumbers() {
        SumOfTwoNumbers sotn = new SumOfTwoNumbers();
        List<Integer> nums = Arrays.asList(12,2);
        assertEquals(14, sotn.calulate(nums));
    }

}
package com.xioq;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class AlgorithmServiceTest {

    private AlgorithmService service = new AlgorithmService();

    @Test
    public void testFibonacci() {
        long expected = 5;
        long actual = service.fibonacci(5);
        assertEquals(expected, actual);
    }

    @Test
    public void testFibonacciOptimized() {
        List<Long> expected = Arrays.asList(0L, 1L, 1L, 2L, 3L, 5L, 8L, 13L, 21L, 34L);
        List<Long> actual = service.fibonacciOptimized(9);
        assertEquals(expected, actual);
    }

    @Test
    public void testFibonacciLastNum() {
        int expected = 2;
        int actual = service.fibonacciLastNum(3);
        assertEquals(expected, actual);
    }

    @Test
    public void testFibonacciLastNumBigger() {
        int expected = 9;
        int actual = service.fibonacciLastNum(331);
        assertEquals(expected, actual);
    }
    @Test
    public void testFibonacciLastNumHuge() {
        int expected = 5;
        int actual = service.fibonacciLastNum(327305);
        assertEquals(expected, actual);
    }
    @Test
    public void testGreatestCommonFactor() {
        long expected = 3;
        long actual = service.greatestCommonFactor(33, 27);
        assertEquals(expected, actual);
    }

    @Test
    public void testGreatestCommonFactorReversed() {
        long expected = 3;
        long actual = service.greatestCommonFactor(27, 33);
        assertEquals(expected, actual);
    }

    @Test
    public void testLowestCommonMultiple() {
        long expected = 12;
        long actual = service.findLowestCommonMultiple(3, 4);
        assertEquals(expected, actual);
    }
    @Test
    public void testLowestCommonMultipleSample1() {
        long expected = 24;
        long actual = service.findLowestCommonMultiple(6, 8);
        assertEquals(expected, actual);
    }
    @Test
    public void testLowestCommonMultipleSample2() {
        long expected = 1933053046;
        long actual = service.findLowestCommonMultiple(28851538, 1183019);
        assertEquals(expected, actual);
    }
}
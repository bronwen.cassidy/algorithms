package com.xioq;

import java.util.List;

public class MaximumPairwiseProduct {

    public Long execute (List<Integer> nums) {
        long maxProduct = 0;
        int lh = 0;
        int rh = 1;
        while (rh < nums.size()) {
            Integer first = nums.get(lh++);
            Integer second = nums.get(rh++);
            long total = first * second;
            if (total > maxProduct) {
                maxProduct = total;
            }
        }
        return maxProduct;
    }
}

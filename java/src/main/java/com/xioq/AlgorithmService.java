package com.xioq;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.LongStream;

public class AlgorithmService {

    public long fibonacci(int n) {
        if (n <= 1) return n;
        return fibonacci(n - 1) + fibonacci(n - 2);
    }

    public List<Long> fibonacciOptimized(int n) {
        // NOTE: Arrays.asList created a fixed sized list you cannot ADD to it!!
        final List<Long> initial = new ArrayList<>(Arrays.asList(0L, 1L, 1L));
        LongStream.range(2, n).forEach(e -> {
            Long first = initial.get(initial.size() - 1);
            Long second = initial.get(initial.size() - 2);
            initial.add(first + second);
        });

        return initial;
    }

    /**
     * compute a huge fibanacci number modulo m, too big to fit in a number
     * hence the modulo 10
     */
    public int fibonacciLastNum(int n) {
        // NOTE: Arrays.asList created a fixed sized list you cannot ADD to it!!
        final List<Integer> initial = new ArrayList<>(Arrays.asList(0, 1, 1));
        LongStream.range(2, n).forEach(e -> {
            Integer first = initial.get(initial.size() - 1);
            Integer second = initial.get(initial.size() - 2);
            int num = (first + second) % 10;
            initial.add(num);
        });

        return initial.get(initial.size() - 1);
    }

    /**
     * ;; The Euclidean algorithm which finds the highest common factor by using the modulus function
     * ;; so swap a and b if a is greater find the remainder of dividing the larger number by the smaller
     * ;; and then use the smaller num,ber as the bigger number and the remainder of the modulo operation
     * ;; as the smaller number and continue until the smaller number == 0;
     */
    public Long greatestCommonFactor(long a, long b) {
        if (b == 0) return a;
        if (a > b) {
            long temp = a;
            a = b;
            b = temp;
        }
        return greatestCommonFactor(a, (b % a));
    }

    public long findLowestCommonMultiple(long a, long b) {
        return (a * b) / greatestCommonFactor(a, b);
    }

    public long largestNumber(Integer[] numbers) {

        Arrays.sort(numbers, (lhs, rhs) -> {
            String left = lhs.toString();
            String right = rhs.toString();
            return (left + right).compareTo(right + left) * -1;
        });
        StringBuilder b = new StringBuilder();
        for (Integer number : numbers) {
            b.append(number);
        }
        return Long.parseLong(b.toString());
    }

    // todo finonacci starting at an index to limit so 5 -> 20 need to to undestand what is required
}

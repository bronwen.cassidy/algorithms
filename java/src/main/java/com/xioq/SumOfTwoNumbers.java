package com.xioq;

import java.util.List;

public class SumOfTwoNumbers {

    public int calulate(List<Integer> numbers) {
        return numbers.stream().reduce(0, Integer::sum);
    }
}
